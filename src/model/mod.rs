extern crate colored;

use self::colored::*;

pub mod input;
pub mod weight;
pub mod neural_node;
pub mod image_class;

pub static IMAGE_SIZE: usize = 32;